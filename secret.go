package main

import (
	"crypto/sha256"
	"fmt"
	"strings"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v2"
)

const defaultSha = "0000000"

// Secret represents a gitleaks leak
type Secret struct {
	Line       string `json:"line"`
	LineNumber int    `json:"lineNumber"`
	Offender   string `json:"offender"`
	Rule       string `json:"rule"`
	Commit     string `json:"commit"`
	File       string `json:"file"`
	Message    string `json:"commitMessage"`
	Author     string `json:"author"`
	Date       string `json:"date"`
}

func (s *Secret) identifiers() []report.Identifier {
	return []report.Identifier{
		{
			Type:  report.IdentifierType("gitleaks_rule_id"),
			Name:  fmt.Sprintf("Gitleaks rule ID %s", s.Rule),
			Value: s.Rule,
		},
	}
}

func (s *Secret) location() report.Location {
	commit := report.Commit{
		Author:  s.Author,
		Date:    s.Date,
		Message: s.Message,
		Sha:     s.Commit,
	}
	if commit.Sha == "" {
		commit.Sha = defaultSha
	}
	return report.Location{
		File:      s.File,
		LineStart: s.LineNumber,
		LineEnd:   s.LineNumber,
		Commit:    &commit,
	}
}

func (s *Secret) fingerprint() string {
	// Cleanup the source code extract from the report.
	sourceCode := strings.TrimSpace(s.Offender)
	// create code fingerprint using SHA256.
	h := sha256.New()
	h.Write([]byte(sourceCode))
	return fmt.Sprintf("%x", h.Sum(nil))
}

func (s *Secret) compareKey() string {
	fingerprint := s.fingerprint()
	if fingerprint != "" {
		return strings.Join([]string{s.File, fingerprint, s.Rule}, ":")
	}
	return strings.Join([]string{s.File, s.Rule}, ":")
}

func (s *Secret) message() string {
	return fmt.Sprintf("%s detected; please remove and revoke it if this is a leak.", s.Rule)
}

func (s *Secret) description() string {
	if s.Commit != "" {
		return fmt.Sprintf("Historic %s secret has been found in commit %s.", s.Rule, s.Commit)
	}
	return s.Rule
}
